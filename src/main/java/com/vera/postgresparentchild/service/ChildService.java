/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.postgresparentchild.service;

import com.vera.postgresparentchild.exception.ResourceNotFoundException;
import com.vera.postgresparentchild.model.Child;
import com.vera.postgresparentchild.repository.ChildRepository;
import com.vera.postgresparentchild.repository.ParentRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 *
 * @author Wera
 */
@Service
public class ChildService {
    @Autowired
    private ChildRepository childRepository;
    @Autowired
    private ParentRepository parentRepository;
    
    public List<Child> getAllChilds() {
        return childRepository.findAll();
    }

    public List<Child> getChildsByParentId(Long parentId) {
        return childRepository.findByParentId(parentId);
    }

    public Child addChild( Long parentId, Child child) {
        return parentRepository.findById(parentId)
                .map(parent -> {
                    child.setParent(parent);
                    return childRepository.save(child);
                }).orElseThrow(() -> new ResourceNotFoundException(
                        "Parent not found with id " + parentId));
    }

    public Child updateChild( Long parentId,Long childId,Child childRequest) {
        if(!childRepository.existsById(parentId)) {
            throw new ResourceNotFoundException(
                    "Parent not found with id " + parentId);
        }
        return childRepository.findById(childId)
                .map(child -> {
                    child.setName(childRequest.getName());
                    return childRepository.save(child);
                }).orElseThrow(() -> new ResourceNotFoundException(
                        "Child not found with id " + childId));
    }

    public ResponseEntity<?> deleteChild( Long parentId,Long childId) {
        if(!parentRepository.existsById(parentId)) {
            throw new ResourceNotFoundException(
                    "Parent not found with id " + parentId);
        }
        return childRepository.findById(childId)
                .map(child -> {
                    childRepository.delete(child);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException(
                        "Child not found with id " + childId));

    }
}
