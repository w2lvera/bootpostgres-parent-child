/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.postgresparentchild.service;

import com.vera.postgresparentchild.exception.ResourceNotFoundException;
import com.vera.postgresparentchild.model.Parent;
import com.vera.postgresparentchild.repository.ParentRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Wera
 */
@Service
public class ParentService {

    @Autowired
    ParentRepository parentRepository;

    @Transactional
    public List<Parent> getAllParents() {
        return parentRepository.findAll();
    }

    @Transactional
    public Parent saveParent(Parent parent) {
        return parentRepository.save(parent);
    }

    @Transactional
    public Parent updateParent(Long parentId,
            Parent parentRequest) {
        return parentRepository.findById(parentId)
                .map(parent -> {
                    parent.setName(parentRequest.getName());
                    return parentRepository.save(parent);
                }).orElseThrow(()
                        -> new ResourceNotFoundException(
                                "Parent not found with id " + parentId));
    }

    public ResponseEntity<?> deleteQuestion(Long parentId) {
        return parentRepository.findById(parentId)
                .map(parent -> {
                    parentRepository.delete(parent);
                    return ResponseEntity.ok().build();
                }).orElseThrow(()
                        -> new ResourceNotFoundException(
                                "Parent not found with id " + parentId));
    }

}
