/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.postgresparentchild.controller;
import com.vera.postgresparentchild.exception.ResourceNotFoundException;
import com.vera.postgresparentchild.model.Parent;
import com.vera.postgresparentchild.repository.ParentRepository;
import com.vera.postgresparentchild.service.ParentService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@RestController
public class ParentController {

    @Autowired
    private ParentService parentService;
    @GetMapping("/parents")
    public List<Parent> getParents() {
        return parentService.getAllParents();
    }
    
    @PostMapping("/parents")
    public Parent createParent(@Valid @RequestBody Parent parent) {
        return parentService.saveParent(parent);
    }

    @PutMapping("/parents/{parentId}")
    public Parent updateParent(@PathVariable Long parentId,
                                   @Valid @RequestBody Parent parentRequest) {
        return parentService.updateParent(parentId, parentRequest);
    }

    @DeleteMapping("/parents/{parentId}")
    public ResponseEntity<?> deleteQuestion(@PathVariable Long parentId) {
        return parentService.deleteQuestion(parentId);
    }
}
