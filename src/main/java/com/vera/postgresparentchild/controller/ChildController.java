/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.postgresparentchild.controller;
import com.vera.postgresparentchild.model.Child;
import com.vera.postgresparentchild.service.ChildService;
import com.vera.postgresparentchild.service.ParentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
public class ChildController {

    @Autowired
    private ChildService  childService;

    @Autowired
    private ParentService parentService;
    
     @GetMapping("/childs")
    public List<Child> getAllChilds() {
        return childService.getAllChilds();
    }

    @GetMapping("/childs/{parentId}/childs")
    public List<Child> getChildsByParentId(@PathVariable Long parentId) {
        return childService.getChildsByParentId(parentId);
    }

    @PostMapping("/childs/{parentId}/childs")
    public Child addChild(@PathVariable Long parentId,
                            @Valid @RequestBody Child child) {
           return childService.addChild(parentId, child);
    }
 
    @PutMapping("/childs/{parentId}/childs/{childId}")
    public Child updateAnswer(@PathVariable Long parentId,
                               @PathVariable Long childId,
                               @Valid @RequestBody Child childRequest) {
        return childService.updateChild(parentId, childId, childRequest);
    }

    @DeleteMapping("/childs/{parentId}/childs/{childId}")
    public ResponseEntity<?> deleteChild(@PathVariable Long parentId,
                                          @PathVariable Long childId) {
        return childService.deleteChild(parentId, childId);
    }
       
}