/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.postgresparentchild.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import javax.persistence.*;
/**
 *
 * @author Wera
 */
@Entity
@Table(name = "childs")

public class Child extends AuditModel{
   
    @Id
    @Basic(optional = false)
   
    @Column(name = "id")
    @GeneratedValue(generator = "childs_generator")
    @SequenceGenerator(
            name = "childs_generator",
            sequenceName = "childs_sequence",
            initialValue = 1000
    )
    private Long id;
        
    @Column(name = "name")
    private String name;
    
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "parent_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Parent parent;
  //  @JoinColumn(name = "parent_id", referencedColumnName = "id")
   

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
 
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Parent getParent() {
        return parent;
    }

    public void setParent(Parent parent) {
        this.parent = parent;
    }
 
}
