/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.postgresparentchild.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "parent")

public class Parent extends AuditModel {
   
    @Id
    @GeneratedValue(generator = "parent_generator")
    @SequenceGenerator(
            name = "parent_generator",
            sequenceName = "parent_sequence"
           , initialValue = 1
    )
    
    
    private Long id;
    
    @NotBlank
    @Size(min = 3, max = 100)
    private String name;
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}
